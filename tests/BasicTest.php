<?php

require './vendor/autoload.php';

use CBC\Invoice;
use CBC\Tax;

class BasicTest extends \PHPUnit\Framework\TestCase
{

    /** @test */
    public function testSingleItem()
    {
        $invoice = new Invoice();
        $gst_tax = new Tax();
        $gst_tax->setValue(Invoice::GST);

        $invoice->addItem("Item 1", 123.45, $gst_tax);

        self::assertEquals([
            "subtotal" => 123.45,
            "tax" => 12.34,
            "total" => 135.79
        ], $invoice->getTotals());
    }

    /** @test */
    public function testOnGetItemsBehaviour()
    {
        $invoice = new Invoice();
        $gst_tax = new Tax();
        $gst_tax->setValue(Invoice::GST);

        $invoice->addItem("Item 1", 123.45, $gst_tax);
        $invoice->addItem("Item 2", 100.45, $gst_tax);

        $items = $invoice->getItems();
        self::assertEquals(12.34, $items[0]["tax"]);
        self::assertEquals(10.04, $items[1]["tax"]);
    }

    /** @test */
    public function testTaxFreeMultipleItems()
    {
        $invoice = new Invoice();
        $no_tax = new Tax();
        $no_tax->setValue(Invoice::NONE);

        $invoice->addItem("Item 1", 123.45, $no_tax);
        $invoice->addItem("Item 2", 100.45, $no_tax);

        self::assertEquals([
            "subtotal" => 223.90,
            "tax" => 0.0,
            "total" => 223.90
        ], $invoice->getTotals());
    }

    /** @test */
    public function testTaxFreeSingleItem()
    {
        $invoice = new Invoice();
        $no_tax = new Tax();
        $no_tax->setValue(Invoice::NONE);

        $invoice->addItem("Item 1", 23.45, $no_tax);

        self::assertEquals([
            "subtotal" => 23.45,
            "tax" => 0.0,
            "total" => 23.45
        ], $invoice->getTotals());
    }

    /** @test */
    public function testTwoItems()
    {
        $invoice = new Invoice();
        $gst_tax = new Tax();
        $gst_tax->setValue(Invoice::GST);

        $invoice->addItem("Item 1", 100.00, $gst_tax);
        $invoice->addItem("Item 2", 23.45, $gst_tax);

        self::assertEquals([
            "subtotal" => 123.45,
            "tax" => 12.34,
            "total" => 135.79
        ], $invoice->getTotals());
    }
}