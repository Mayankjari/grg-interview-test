<?php

namespace CBC;

/**
 * Class Invoice
 * @package CBC
 */
class Invoice
{
    const GST = 10;
    const NONE = 0;

    /** @var $items array */
    private $items = [];

    /**
     * @param string $description : Adding item into the invoice
     * @param string $value : Item value
     * @param Tax $tax : Tax value
     */
    public function addItem($description, $value, $tax)
    {
        $this->items[] = [
            "description" => $description,
            "value" => $value,
            "tax" => $tax->getValue()
        ];
    }

    public function getItems()
    {
        $tax_calculated_items = [];
        foreach ($this->items as $item) {
            $tax_calculated_items[] = [
                "description" => $item["description"],
                "tax" => $this->getTwoDecimalValue($item["value"] * ($item["tax"] / 100)),
                "value" => $item["value"]
            ];
        }
        return $tax_calculated_items;
    }

    /**
     * @return float[]
     */
    public function getTotals()
    {
        $tax_total = 0.0;
        $sub_total = 0.0;
        foreach ($this->getItems() as $item) {
            $tax_total += $item["tax"];
            $sub_total += $item["value"];
        }
        return [
            "subtotal" => $this->getTwoDecimalValue($sub_total),
            "tax" => $this->getTwoDecimalValue($tax_total),
            "total" => $this->getTwoDecimalValue($sub_total + $tax_total),
        ];
    }

    private function getTwoDecimalValue($value)
    {
        return intval(($value * 100)) / 100;
    }

}