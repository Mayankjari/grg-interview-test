<?php

namespace CBC;

/**
 * Class Invoice
 * @package CBC
 */
class Tax
{
    /** @var $tax */
    private $value;

    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }


}