<?php

// Allowing to use namespaces
require "./vendor/autoload.php";

use CBC\Invoice;
use CBC\Tax;

$invoice = new Invoice();
$tax = new Tax();

$tax->setValue(Invoice::GST);
$invoice->addItem("Pantry Shelves", 1004.56, $tax);
$tax->setValue(Invoice::NONE);
$invoice->addItem("Hinges", 24.56, $tax);
$tax->setValue(Invoice::GST);
$invoice->addItem("Door handles", 12.45, $tax);

echo "<table border='2' cellpadding='10'>";
echo "<tr><th>Item Description</th><th>Item Value</th><th>Tax on Amount</th></tr>";
/** @var array $item */
foreach ($invoice->getItems() as $item) {
    echo "<tr>";
    echo "<td>" . $item["description"] . "</td>";
    echo "<td>" . $item["value"] . "</td>";
    echo "<td>" . $item["tax"] . "</td>";
    echo "</tr>";
}
echo "</table>";
